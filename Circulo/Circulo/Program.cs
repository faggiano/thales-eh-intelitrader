﻿using System;
namespace Intelitrade {
    class Circulo {
		private double raioCirculo = 32;
		private double borda = 20;
		private double excentricidade = 0.55;
		
		public void Iniciar() {
           	Console.Clear();
           	Console.WriteLine("Desenhando Circulo!\n");
			Console.ResetColor();
			Console.ForegroundColor = ConsoleColor.Blue;

			Desenhar();
			Console.ResetColor();
		}

		public void Desenhar() {
			for(double count=raioCirculo; count>1; count=count*(0.0+excentricidade)){
				double espaco = raioCirculo - count;
				for (double coun=0; coun<=borda; coun++)
					Console.Write(" "); 
				imprime(espaco,"*");
				imprime(count," ");
				Console.Write("\n");
			} 
			for(double count=1; count<raioCirculo; count=count*(1.0+excentricidade)){
				double espaco = raioCirculo - count;
				for (double coun=0; coun<=borda; coun++)
					Console.Write(" "); 
				imprime(espaco,"*");
				imprime(count," ");
				Console.Write("\n");
			} 
		}

		public void imprime(double x, string y){
			for(double count=0; count<=x; count++){
				Console.Write(y);
			}
		}
    }

	class Program {
        static void Main(string[] args) {
			var circulo = new Circulo();
			circulo.Iniciar();
		}
	}
}

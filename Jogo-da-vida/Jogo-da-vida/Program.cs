﻿using System;
using System.Threading;

namespace intelitrader
{
    class Jogo
    {
        private bool[,] mundo;

        private int larguraMundo = 60;
        private int alturaMundo = 30;
        private int offsetEsquerda = 2;
        private int offsetTopo = 2;

        public void Iniciar()
        {
            Console.Clear();
            Console.WriteLine("Jogo, iniciado!");
            Console.ResetColor();
            Console.ForegroundColor = ConsoleColor.Blue;
            
            CriarInfinito();
            Desenhar();

            while(true)
            {
                instante();

                if(Console.KeyAvailable)
                {
                    var infoTecla = Console.ReadKey(true);
                    if(infoTecla.Key == ConsoleKey.Spacebar)    { SomarAleatoriedade(); }
                    else if(infoTecla.Key == ConsoleKey.R)      { Iniciar(); break; }
                    else if(infoTecla.Key == ConsoleKey.Q)      { CriarInfinito(); }
                    else if(infoTecla.Key == ConsoleKey.W)      { CriarAleatoriedade(); }
                    else if(infoTecla.Key == ConsoleKey.Escape) { break; }
                }
            }
            Console.BackgroundColor = ConsoleColor.Black;
            Console.SetCursorPosition(1, offsetTopo+alturaMundo+3);
        }

        public void Desenhar()
        {
            for(int j=0; j < alturaMundo; j++) {
                for(int i=0; i < larguraMundo; i++) {
                    atualizarDesenho(i, j);
                }
            }
        }

        public void atualizarDesenho(int x, int y) {
            atualizarDesenho(x, y, mundo[x, y]);
        }

        public void apagarMundo() 
        {
            mundo = new bool[larguraMundo, alturaMundo];
            Desenhar();
        }

        public void CriarInfinito() 
        {
            apagarMundo();
            
            int x = larguraMundo/2 - 5;
            int y = alturaMundo/2 - 2;

            Add(x, y);    Add(x+2, y);  Add(x+4, y);
            Add(x, y+1);                Add(x+4, y+1);
            Add(x, y+2);                Add(x+4, y+2);
            Add(x, y+3);                Add(x+4, y+3);
            Add(x, y+4); Add(x+2, y+4); Add(x+4, y+4);
        }

        public void CriarAleatoriedade()
        {
            apagarMundo();
            for(int i=0; i < larguraMundo; i++) {
                SomarAleatoriedade();
            }
        }
        
        public void atualizarDesenho(int x, int y, bool colored)
        {
            Console.BackgroundColor = colored ? ConsoleColor.Blue : ConsoleColor.Yellow;
            Console.SetCursorPosition(offsetEsquerda+x, offsetTopo+y);
            Console.Write(" ");
            Console.ResetColor();
        }

        public void instante()
        {
            Thread.Sleep(1000/4);

            var copiaMundo = (bool[,])mundo.Clone();
            for(int i=0; i < larguraMundo; i++)
            {
                for(int j=0; j < alturaMundo; j++)
                {   
                    if(copiaMundo[i, j])
                    {
                        int c = ContarVizinhos(i, j, false);
                        if(c <= 1 || c >= 4){
                            copiaMundo[i, j] = false;
                            atualizarDesenho(i, j, false);
                        }
                        else {
                            copiaMundo[i, j] = true;
                            atualizarDesenho(i, j, true);
                        }
                    }
                    else
                    {
                        int c = ContarVizinhos(i, j, true);
                        if(c == 3){
                            copiaMundo[i, j] = true;
                            atualizarDesenho(i, j, true);
                        }
                    }
                }
            }

            mundo = copiaMundo;
        }

        public void SomarAleatoriedade()
        {
            Random rand = new Random();
            Add(rand.Next(larguraMundo), rand.Next(alturaMundo));
        }

        public void Add(int x, int y){
            mundo[x, y] = true;
            atualizarDesenho(x, y, true);
        }

        public int ContarVizinhos(int px, int py, bool countEmpty)
        {
            int count = 0;
            for(int i=-1; i <= 1; i++){
                for(int j=-1; j <= 1; j++)
                {
                    int x = px - i, y = py - j;
                    if((x == px && y == py) || x < 0 || y < 0 || x >= larguraMundo || y >= alturaMundo){ 
                        continue;
                    }

                    if((!countEmpty && mundo[x, y]) || (countEmpty && mundo[x, y])){
                        count++;
                    }
                }
            }
            return count;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var jogo = new Jogo();
            jogo.Iniciar();
        }
    }
}

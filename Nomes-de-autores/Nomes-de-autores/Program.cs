﻿/*
Autor: Thales Faggiano
Contato: faggiano@creci.org.br

Know issues:
- Validação dos campos;
- Formatação dos nomes conforme regras dos exercícios;
- Falta criar testes unitários;

WIP 06/04/2020 6:00 PM:
- Formatação dos nomes conforme regras dos exercídios;
*/
using System;

namespace Intelitrader
{
    class Program
    {
        static void Main(string[] args)
        {
			Console.Clear();
			Console.WriteLine("Seja bem-vindo!");
       		Console.WriteLine("Por gentileza, forneça o número de autores que o programa deverá capturar: ");
			int numeroAutores = int.Parse(Console.ReadLine());
			int count = 0;
			string[] nomeAutores = new string[numeroAutores];
			while ( count < numeroAutores )
			{
				Console.WriteLine("Digite o nome do autor: ");
				nomeAutores[count] = Console.ReadLine();
				nomeAutores[count] = Formatando(nomeAutores[count]);
				count++;
			}
			
			foreach(var nomeFormatado in nomeAutores)
				Console.WriteLine("Você digitou: " + nomeFormatado);
        }

		static string Formatando(string i)
		{
			//Console.WriteLine("O " + i + " passou por aqui");
			string[] nomes = i.Split(' ');
			string ultimoNome = nomes[nomes.Length - 1];
			string primeiroNome = "";
			for(int count = 0; count < nomes.Length - 1; count++) 
			{
				primeiroNome = primeiroNome + nomes[count];
			}
			return ultimoNome.ToUpper() + ", " + primeiroNome;
		}
    }
}
